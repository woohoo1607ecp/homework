import {controls} from "../../constants/controls";
import {changeHealth, getCriticalHit, getDamage, isCriticalHit} from "../components/fight";

export function fightEventListener (resolve, pressed, firstFighter, secondFighter, firstHealth, secondHealth, canFirstFighterHitCritical, canSecondFighterHitCritical, firstFighterHealthElement, secondFighterHealthElement) {
  document.addEventListener('keydown', function(event) {
    pressed.add(event.code);
    let damage;

    if (pressed.has(controls.PlayerOneAttack) && pressed.has(controls.PlayerTwoBlock)) {

      damage = getDamage(firstFighter, secondFighter);
      secondHealth = changeHealth(secondFighter.health, secondHealth, damage, secondFighterHealthElement);

    } else if (pressed.has(controls.PlayerTwoAttack) && pressed.has(controls.PlayerOneBlock)) {

      damage = getDamage(secondFighter, firstFighter);
      firstHealth = changeHealth(firstFighter.health, firstHealth, damage, firstFighterHealthElement);

    } else if (pressed.has(controls.PlayerOneAttack) && !pressed.has(controls.PlayerOneBlock)) {

      damage = getDamage(firstFighter);
      secondHealth = changeHealth(secondFighter.health, secondHealth, damage, secondFighterHealthElement);

    } else if (pressed.has(controls.PlayerTwoAttack) && !pressed.has(controls.PlayerTwoBlock)) {

      damage = getDamage(secondFighter);
      firstHealth = changeHealth(firstFighter.health, firstHealth, damage, firstFighterHealthElement);

    } else if (isCriticalHit(pressed, controls.PlayerOneCriticalHitCombination) && canFirstFighterHitCritical) {

      damage = getCriticalHit(firstFighter);
      secondHealth = changeHealth(secondFighter.health, secondHealth, damage, secondFighterHealthElement);
      canFirstFighterHitCritical = false;
      setTimeout(()=>{canFirstFighterHitCritical = true}, 3000);

    } else if (isCriticalHit(pressed, controls.PlayerTwoCriticalHitCombination) && canSecondFighterHitCritical) {

      damage = getCriticalHit(secondFighter);
      firstHealth = changeHealth(firstFighter.health, firstHealth, damage, firstFighterHealthElement);
      canSecondFighterHitCritical = false;
      setTimeout(()=>{canSecondFighterHitCritical = true}, 3000);
    }

    if (firstHealth<=0) {
      resolve(secondFighter)
    }
    if (secondHealth<=0) {
      resolve(firstFighter)
    }

  });
}
