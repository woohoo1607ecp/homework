import { controls } from '../../constants/controls';
import {fightEventListener} from "../services/fightEventListener";

export async function fight(firstFighter, secondFighter) {
  return new Promise( (resolve) => {
    // resolve the promise with the winner when fight is over
    let firstHealth = firstFighter.health;
    let secondHealth = secondFighter.health;
    let canFirstFighterHitCritical = true;
    let canSecondFighterHitCritical = true;

    let pressed = new Set();

    const firstFighterHealthElement = document.getElementById('left-fighter-indicator');
    const secondFighterHealthElement = document.getElementById('right-fighter-indicator');

    fightEventListener(resolve, pressed, firstFighter, secondFighter,
        firstHealth, secondHealth, canFirstFighterHitCritical,
        canSecondFighterHitCritical, firstFighterHealthElement, secondFighterHealthElement);


    document.addEventListener('keyup', function(event) {
      pressed.delete(event.code);
    });

  });
}

function getRandom() {
  return Math.random()*2
}

export function getDamage(attacker, defender) {
  // return damage
  let damage;
  if (!defender) {
    damage = getHitPower(attacker);
  } else {
    damage = getHitPower(attacker)-getBlockPower(defender);
  }
  return damage>0 ? damage : 0
}

export function getHitPower(fighter) {
  return fighter.attack*getRandom()
  // return hit power
}

export function getBlockPower(fighter) {
  return fighter.defense*getRandom()
  // return block power
}

export function getCriticalHit(fighter) {
  return fighter.attack*2
}


export function getHealthPercents(fullHealth, nowHealth) {
  let percents = (100/fullHealth)*nowHealth;
  return percents>0 ? percents+'%' : '0%';
}
export function changeHealth(fullHealth, nowHealth, damage, element) {
  nowHealth = nowHealth-damage;
  element.style.width = getHealthPercents(fullHealth, nowHealth);
  return nowHealth
}
export function isCriticalHit(pressed, combination) {
  for (let code of combination) {
    if (!pressed.has(code)) {
      return false;
    }
  }
  return true
}
